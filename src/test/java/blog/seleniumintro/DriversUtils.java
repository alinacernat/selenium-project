package blog.seleniumintro;

import org.apache.commons.lang3.SystemUtils;

public class DriversUtils {

    public static void setWebdriverProperty(String browser) {
        switch (browser) {
            case "chrome":
                System.setProperty("webdriver.chrome.driver", getDriversPath()+ "chromedriver"+ getFileExtension());
                break;
            case "firefox":
                System.setProperty("webdriver.gecko.driver", getDriversPath()+ "geckodriver"+ getFileExtension());
        }
    }

    private static String getProjectPath(){
        return System.getProperty("user.dir");
    }

    private static String getDriversPath(){

        String driversPath;
        if(SystemUtils.IS_OS_WINDOWS){
            driversPath = "\\src\\test\\resources\\drivers\\windows\\";
        } else{
            driversPath = "\\src\\test\\resources\\drivers\\mac\\";
        }

        return getProjectPath()+ driversPath;

    }

    private static String getFileExtension(){
        if (SystemUtils.IS_OS_WINDOWS){
            return ".exe";
        }
        return "";
    }
}
