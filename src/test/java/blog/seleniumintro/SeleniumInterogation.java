package blog.seleniumintro;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static blog.seleniumintro.DriversUtils.setWebdriverProperty;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class SeleniumInterogation {

    private static WebDriver driver;

    @BeforeClass
    public static void setup() {
        setWebdriverProperty("chrome");
        driver = new ChromeDriver();
    }

    @Test
    public void pageSourceTest() throws InterruptedException {
        driver.get("https://practica.wantsome.ro/blog");
        System.out.println("Current url is: " + driver.getCurrentUrl());
        System.out.println("Current title is: " + driver.getTitle());
    }

    @Test
    public void testTitle() {
        driver.get("https://practica.wantsome.ro/blog");

        String title = driver.getTitle();
        String[] words = title.split(" ");
        int numberOfWordsWithMoreThan5Letters = 0;

        for (String word : words) {
            if (word.length() > 5) {
                numberOfWordsWithMoreThan5Letters++;
            }
        }

        assertEquals(5, numberOfWordsWithMoreThan5Letters);
    }


    @Test
    public void testTextPresentOnSourcePage() {
        driver.get("https://practica.wantsome.ro/blog");
        assertTrue(driver.getPageSource().contains("The roof is on fireeee"));
    }


        @AfterClass
        public static void teardown () {
            driver.close();
        }
    }

