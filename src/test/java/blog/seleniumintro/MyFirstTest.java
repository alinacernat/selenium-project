package blog.seleniumintro;

import org.apache.commons.lang3.SystemUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import static blog.seleniumintro.DriversUtils.setWebdriverProperty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class MyFirstTest {
    @Test
    public void openChromeBrowser() throws InterruptedException {
        setWebdriverProperty("chrome");
        WebDriver driver = new ChromeDriver();

        driver.get("https://practica.wantsome.ro/blog");
        assertEquals("Wanstsome Iasi- Practice website for testing sessions", driver.getTitle());
        driver.navigate().to("https://www.emag.ro");
        assertNotEquals("Wanstsome Iasi- Practice website for testing sessions", driver.getTitle());
        assertEquals("eMAG - Libertate in fiecare zi", driver.getTitle());

        String pageTitle = driver.getTitle();
        System.out.println(pageTitle);

        Thread.sleep(5000);
        assertEquals("Wantsome Iasi – Practice website for testing sessions", driver.getTitle());
        //tear down
        driver.close();
    }


    @Test
    public void openFirefoxBrowser() throws InterruptedException {
        setWebdriverProperty("firefox");
        WebDriver driver = new FirefoxDriver();

        driver.get("https://practica.wantsome.ro/blog");

        String pageTitle = driver.getTitle();
        System.out.println(pageTitle);

        Thread.sleep(5000);
        assertEquals("Wantsome Iasi – Practice website for testing sessions", driver.getTitle());
        //tear down
        driver.close();
    }

    @Test

    public void openHtmlUnitBrowser() throws InterruptedException {

        WebDriver driver = new HtmlUnitDriver();
        driver.get("https://practica.wantsome.ro/blog");

        String pageTitle = driver.getTitle();
        System.out.println(pageTitle);

        Thread.sleep(5000);
        assertEquals("Wantsome Iasi – Practice website for testing sessions", driver.getTitle());
        //tear down
        driver.close();
//Html unit nu are interfata grafica dar testul trece
    }

    private static WebDriver driver;

    @BeforeClass
    public static void setup() {
        setWebdriverProperty("chrome");
        driver = new ChromeDriver();
    }

    @Test
    public void OpenChromeBrowser() throws InterruptedException {
        driver.get("https://practica.wantsome.ro/blog");
        assertEquals("Wantsome Iasi – Practice website for testing sessions", driver.getTitle());

        driver.navigate().to("https://www.emag.ro");
        assertNotEquals("Wantsome Iasi – Practice website for testing sessions", driver.getTitle());
        assertEquals("eMAG.ro - Libertate în fiecare zi", driver.getTitle());

        driver.navigate().back();
        assertEquals("Wantsome Iasi – Practice website for testing sessions", driver.getTitle());
    }

    @AfterClass
    public static void teardown() {
        driver.close();
    }
}


